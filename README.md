# Lambda Layer Pipeline Automator

This repo contains a Gitlab pipeline definition and example folder layout ([layers-example](layers-example)) for use in deploying the infrastructure needed to build and publish other infrastructure projects(Lambda Functions/Layers, Cloudformation, etc). Using Terraform in a GitOps-style implementation, the pipeline deploys(or destroys) the infrastructure when the configuration is pushed to a predefined folder.

## Prerequisites

You'll need to have a few things prepared in order to set up everything required for this project.

* A GitLab account(free tier is totally fine)
* An Amazon Web Services account(ideally under the free tier so it doesn't cost you anything to try!)
* The credentials for an IAM user with programmatic access to said AWS account(note that this user needs permission to create all of the necessary resources - https://aws.amazon.com/iam/details/manage-permissions/)

## Installation

### GitLab setup

1. Fork this repo(or clone it and push to your own empty repo)
2. Create a CI job token in the repo containing your Terraform code
3. Set the necessary environment variables(see next section)
4. Commit the Terraform config and vars to an appropriate folder, and watch the magic happen!

### Merging upstream to get updates
This repo may be updated from time to time. If you would like to pull new changes, executing the following terminal commands will do so:
1. git remote add upstream git@gitlab.com:jamescarignan/pipeline-automator.git
2. git fetch upstream
2. git merge upstream/master

While this upstream repo should never have a projects folder, make sure to check before you merge!

### Environment Variables
The CI pipeline relies on some environment variables to fill out the template:
* **ARTIFACT_BUCKET_NAME**: Name of the artifact bucket
* **ARTIFACT_FILENAME**: Name of zip file containing bootstrap module to download/install(i.e. `git_commit_parser`)
* **ARTIFACT_PATH**: S3 path in artifact bucket to download(i.e. `dev/git_commit_parser/v0.0.1`)
* **AWS_ACCESS_KEY_ID**: The AWS_ACCESS_KEY_ID of the IAM user being used
* **AWS_SECRET_ACCESS_KEY**: The AWS_SECRET_ACCESS_KEY of the IAM user being used
* **AWS_DEFAULT_REGION**: The region into which your infrastructure is being deployed 
* **BRANCH_NAME**: Name of the branch to check out from $GITLAB_REPO
* **CI_JOB_TOKEN**: The job token you created earlier for the Terraform infrastructure repository
* **CI_REPO_USERNAME**: The username associated with the job token above
* **GITLAB_REPO**: The name of the GitLab repository containing the Terraform infrastructure code
* **GITLAB_USER**: The name of the Gitlab user for the above repository
* **PROJECT_FOLDER_PATH**: Path to the top-level folder to watch for changes(i.e. if your apps were in `projects/app1` and `projects/app2`, set to `projects`)
* **TF_BUCKET_NAME**: Name of the S3 bucket used to store Terraform state files.
* **TF_BUCKET_REGION**: AWS region where the Terraform state bucket resides.

Note that you should probably mark as protected some of the sensitive variables, like AWS_SECRET_ACCESS_KEY(and maybe AWS_ACCESS_KEY_ID) as well as CI_JOB_TOKEN. This way they don't show up in the pipeline's execution logs.

## Configuration

### Project configuration files

Each project type has its own folder under `PROJECT_FOLDER_PATH`, as seen in the example([layers-example](layers-example)). In this instance, setting the `PROJECT_FOLDER_PATH` environment variable to `layers-example` will tell the pipeline to watch that example, meaning you can make changes to the files in the `requests` folder to see it in action. The contents of a Layer folder are a `terraform.tfvars` file containing the configuration passed into the Terraform module being cloned. Future per-project config can be stored here as well.

## Running the pipeline

Every time a file is committed to this repository under `PROJECT_FOLDER_PATH`, the CI pipeline runs through three stages: Validate, Plan and Apply/Destroy. The execution of each of those is preceded by a bootstrapping phase(i.e. `before_script`).

### Bootstrapping

The `before_script` block runs prior to each stage, which does the following:

* cleans up terraform state and vars
* clones the infrastructure repo 
* runs a python script to parse the commit, and determine whether to render the backend Terraform template and what action to take(apply or destroy)
* enters the directory and performs `terraform init`

### Stages

Every time a file is committed to this repository, the CI pipeline runs through three stages: Validate, Plan and Apply.

#### Validate

Runs `terraform validate` if this is an apply action.

#### Plan

Runs `terraform plan` if this is an apply action. Depends on the **Validate** stage's success, otherwise it is skipped. Creates a `planfile` in the top-level directory.

#### Apply/Destroy

Runs `terraform apply` if this is an apply action, or `terraform destroy` if this is a destroy action. Depends on the **Plan** stage's success, otherwise it is skipped. Uses the `planfile` created in the **Plan** stage as the input for apply.

## TODO

Some issues still remain:

* The `planfile` may need to be cached between runs explicitly(via the `cache` declaration inside a job), though this appears to be unnecessary at this time
* Currently the `apply` and `destroy` jobs run in parallel(though terraform itself only executes in one of them), even though only one is required, because there's no way to conditionally run a job based on information inside the execution context(i.e. setting an environment variable). This means wasted pipeline minutes(about 30s for a single job) but this isn't likely to get triggered often unless under active development
* No branch or merge request config exists
* The python script will only process the first change in the changelist, though this is by design: while a more versatile solution could loop through the entire changelist, there's no way to loop through jobs/stages multiple times. So that logic would have to be folded back into a single step that performs multiple terraform runs. I like the separation of concerns here, where each job is clear in its purpose. It's recommended that you use merge requests onto your active branch anyway, in order to gate changes

This code should not be considered production-ready for the above reasons.

## Notes

* This pipeline deploys one set of infrastructure per project, and CodePipeline can only do a single concurrent run. If you need high-volume/horizontally-scalable builds and deployments, you may want to consider segmenting the same kind of project(Lambda Function, Layer, CFN templates, etc) into multiple folders(per environment, per region, etc).

## License
[MIT](https://choosealicense.com/licenses/mit/)
