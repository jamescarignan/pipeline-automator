import os
import pathlib
import unittest
from git import Repo
from shutil import rmtree
from unittest.mock import patch
from . import determine_terraform_action


class DetermineTerraformActionTests(unittest.TestCase):
    def setUp(self):
        # Make sure that root_folder_path doesn't collide with an existing directory, or it may get erased!
        self.changelist = {}
        self.root_folder_path = "test_projects"
        os.chdir('/tmp')
        self.dir_path = os.getcwd()
        self.repo_base = 'repo_base'
        self.repo_path = '/private/tmp/determine_terraform_action'
        self.repo = Repo.init(self.repo_path)
        print("self.dir_path: " + os.getcwd())
        pathlib.Path(self.dir_path + '/' + self.root_folder_path + '/test_project').mkdir(parents=True, exist_ok=True)
        pathlib.Path(self.dir_path + '/' + self.repo_base).mkdir(parents=True, exist_ok=True)

    def tearDown(self):
        rmtree(self.dir_path + '/' + self.root_folder_path)
        rmtree(self.dir_path + '/' + self.repo_base)
        rmtree(self.repo_path)
        os.remove(self.dir_path + '/' + 'backend.tf')

    def test_should_create_apply_file(self):
        """
        Given a changeset input of change type 'A', create a terraform_apply file.
        """
        tf_file = self.dir_path + '/' + self.root_folder_path + '/test_project/terraform.tfvars'
        print(f"tf_file: {tf_file}")
        with open(tf_file, 'x'):
            pass
        self.changelist[f"{self.root_folder_path}/test_project/test.txt"] = "A"
        with patch.dict('os.environ', {
            'TF_BUCKET_NAME': 'tf-bucket',
            'TF_BUCKET_REGION': 'us-west-2',
            'GITLAB_REPO': 'repo_base',
            'REPO_PATH': '/tmp'
        }):
            determine_terraform_action.determine_terraform_action(self.changelist, self.repo, root_folder_path=self.root_folder_path)
            self.assertTrue(os.path.isfile(os.getcwd() + '/terraform_apply'))
        os.remove(self.dir_path + '/terraform_apply')

    def test_should_create_destroy_file(self):
        """
        Given a changeset input of change type 'A', create a terraform_apply file.
        """
        # Write out project files
        tf_file = self.dir_path + '/' + self.root_folder_path + '/test_project/terraform.tfvars'
        open(tf_file, 'x').close()

        # Commit then delete a file, so that the rollback in git_commit_parser.parse_commit works
        index = self.repo.index
        print(f'self.repo.working_tree_dir: {self.repo.working_tree_dir}')
        new_file_path = os.path.join(self.repo.working_tree_dir, 'test.txt')
        open(new_file_path, 'w').close()
        print(f'new_file_path: {new_file_path}')
        index.add([new_file_path])
        index.commit("add files")
        index.remove([new_file_path], working_tree=True)
        index.commit("delete files")

        # Run the function
        self.changelist[f"{self.root_folder_path}/test_project/test.txt"] = "D"
        with patch.dict('os.environ', {
            'TF_BUCKET_NAME': 'tf-bucket',
            'TF_BUCKET_REGION': 'us-west-2',
            'GITLAB_REPO': 'repo_base',
            'REPO_PATH': '/tmp'
        }):
            determine_terraform_action.determine_terraform_action(self.changelist, self.repo, root_folder_path=self.root_folder_path)
            self.assertTrue(os.path.isfile(os.getcwd() + '/terraform_destroy'))
        os.remove(self.dir_path + '/' + 'terraform_destroy')
