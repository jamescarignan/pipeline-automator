'''
This script looks at the status of an item in the changelist of the commit, renders the backend template,
copies the files necessary for the pipeline, and determines the terraform action to be done.
'''
import os
import subprocess
import sys
from git import Repo
from jinja2 import Environment, FileSystemLoader
from git_commit_parser import parse_commit

# Constants
EMPTY_TREE_SHA = "4b825dc642cb6eb9a060e54bf8d69288fbee4904"


def render_template(project_name):
    """Renders the backend config template to a file"""
    CURRENT_DIR = os.environ.get('REPO_PATH') if 'REPO_PATH' in os.environ.keys() else os.getcwd()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    j2_env = Environment(loader=FileSystemLoader(dir_path), trim_blocks=True)
    rendered_template = j2_env.get_template(f'backend.tf.jinja2').render(
        tf_bucket_name=os.environ.get("TF_BUCKET_NAME"),
        tf_bucket_region=os.environ.get("TF_BUCKET_REGION"),
        project_name=project_name
    )

    with open(f"{CURRENT_DIR}/backend.tf", "w") as outfile:
        outfile.write(rendered_template)


def copy_terraform_files(project_path):
    """Copies the files needed by terraform to its checked out folder"""
    CURRENT_DIR = os.environ.get('REPO_PATH') if 'REPO_PATH' in os.environ.keys() else os.getcwd()
    backend_path = f'backend.tf'
    tfvars_path = f'{project_path}/terraform.tfvars'
    gitlab_repo_path = f'{CURRENT_DIR}/' + os.environ.get('GITLAB_REPO')
    cmd_list = ['cp', backend_path, tfvars_path, gitlab_repo_path]
    subprocess.check_output(cmd_list)


def determine_terraform_action(changelist, repo, root_folder_path=""):
    """
    Get the first file in the changelist.
    Render the terraform backend config template to a file.
    Copy the backend config file, along with the tfvars file to the checked out repo folder.
    Then, based on change type, determine which terraform action to take.
    """
    CURRENT_DIR = os.environ.get('REPO_PATH') if 'REPO_PATH' in os.environ.keys() else os.getcwd()
    if not repo.bare:
        count = 0
        for filename, change_type in changelist.items():
            # Only process the first file in the changelist, as we can't perform multiple terraform actions in the pipeline.
            if count < 1:
                print("===Processing===")
                print("filename: {}".format(filename))
                print("Change type: {}".format(change_type))
                count += 1
                project_name_array = filename.split('/')
                if (len(project_name_array) == 1 and root_folder_path != '') or \
                   (len(project_name_array) > 1 and root_folder_path not in "/".join(project_name_array[0:-1])):
                    # If there's no subfolder, and root_folder_path isn't the top-level folder, we can ignore it.
                    print("changelist_path: " + "/".join(project_name_array[0:-1]))
                    print('Change not in the project folder, ignoring.')
                else:
                    project_name = filename.split('/')[1]
                    if change_type == 'D':
                        # Files got deleted in the last commit, but we need the tfvars file for the destroy. Roll back to get it.
                        repo.git.checkout('HEAD~1')
                    project_path = f'{CURRENT_DIR}/{root_folder_path}/{project_name}'
                    print(f'project_path: {project_path}')
                    if not os.path.isfile(project_path + '/terraform.tfvars'):
                        # We need to check that terraform.tfvars exists - if not, bail
                        sys.exit('Your terraform vars file must be named terraform.tfvars')

                    # Render the template and write out the terraform action file
                    render_template(project_name)
                    copy_terraform_files(project_path)
                    print("deciding terraform action...")
                    if change_type == 'D':
                        with open('terraform_destroy', 'w') as outfile:
                            print("destroy selected...")
                            outfile.write(project_name)
                    elif change_type in ['A', 'M']:
                        with open('terraform_apply', 'w') as outfile:
                            print("apply selected...")
                            outfile.write(project_name)
                    else:
                        print('Should not get here!')
                        sys.exit('Unrecognized change type.')
            # Print out the skipped files
            else:
                print("Skipped {}".format(filename))
    else:
        print('Error loading repository at {}'.format(current_dir))


if __name__ == "__main__":
    current_dir = os.getcwd()
    repo = Repo(current_dir)
    root_folder_path = os.environ["PROJECT_FOLDER_PATH"]

    if not repo.bare:
        print('Repo loaded at {}, parsing last commit...'.format(current_dir))
        commit = repo.head.commit
        changelist = parse_commit(commit, root_folder_path=root_folder_path)
        print(f"changelist: {str(changelist)}")
        determine_terraform_action(changelist, repo, root_folder_path=root_folder_path)
        print('Finished parsing commits')
    else:
        print('Error loading repository at {}'.format(current_dir))
